import { lcv } from "./app.component";

export interface Calibration {
    roi?: any,
    camMatrix?: any,
    mtx?: any,
    dist?: any
}

export interface TargetInfo {
    R: {
        thetaX: number,
        thetaY: number,
        thetaZ: number,
    },
    T: {
        x: number,
        y: number,
        z: number,
    }
}

export interface Point2D {
    x: number, y: number
}

export class CameraUtils {
    private static _instance: CameraUtils;
    public static worker: any;

    public limitIterations: boolean = false;

    public video: any;
    public imageData: any;
    public capture: any;

    public inputImage: any;

    public camCoord: TargetInfo = {
        R: {
            thetaX: 0, thetaY: 0, thetaZ: 0
        },
        T: {
            x: 0, y: 0, z: 0
        }
    }

    public keyboardCoord: TargetInfo = {
        R: {
            thetaX: 0, thetaY: 0, thetaZ: 0
        },
        T: {
            x: 0, y: 0, z: 0
        }
    }

    private markerLength: number = 0.025;
    private keyboardW: number = 0.1;
    private keyboardH: number = 0.405;

    private markers: { [key: number]: TargetInfo } = {};
    private markerPoints: {
        [key: number]: {
            points: Point2D[],
            meanPoint: Point2D
        }
    } = {};



    private dictionary: any;

    private detectorParams: any;

    private keyboardDst: any;
    private keyboardBoard: any;

    private camRvect: any;
    private camTvect: any;

    private images: {
        [key: string]: any
    } = {};

    private calibrations: Calibration = {};

    private initialPosCalibrated: boolean = false;

    private loaded: boolean = false;

    constructor() {
    }

    public static get instance(): CameraUtils {
        if (!CameraUtils._instance)
            CameraUtils._instance = new CameraUtils();

        return CameraUtils._instance;
    }

    /**
     * Called on camera load
     * @param video current video source
     */
    public onLoad(video: any): void {
        this.loadArucoParams();
        this.loadArucoBoard();
        this.loadCalibration();
        this.initImages();

        this.video = video;
        const width: number = 1280;
        const height: number = 720;

        navigator.mediaDevices
            .getUserMedia({
                audio: false,
                video: {
                    width: { exact: width },
                    height: { exact: height },
                    frameRate: { exact: 30 }
                }
            })
            .then((stream) => {
                // Stream the video to the canvas
                if ("srcObject" in this.video) {
                    this.video.srcObject = stream;
                } else {
                    this.video.src = window.URL.createObjectURL(stream);
                }

                // camera translation and rotation matrices
                this.camRvect = new lcv.Mat(1, 3, lcv.CV_64F);
                this.camTvect = new lcv.Mat(1, 3, lcv.CV_64F);

                this.keyboardDst = lcv.matFromArray(4, 2, lcv.CV_32F, [
                    0, 0,
                    this.keyboardW * 1000, 0,
                    this.keyboardW * 1000, this.keyboardH * 1000,
                    0, this.keyboardH * 1000
                ])

                this.loaded = true;

                // Load the calibration values
                this.inputImage = new lcv.Mat(height, width, lcv.CV_8UC4);
                this.capture = new lcv.VideoCapture("video");

                let interval = setInterval(async () => {
                    var t0 = performance.now()
                    this.tick();
                    var t1 = performance.now()
                    console.log("Tick() took " + (t1 - t0) + " milliseconds.")
                }, 100);

                if (this.limitIterations)
                    setTimeout(() => {
                        clearInterval(interval);
                    }, 1000);
            })
            .catch((err) => {
                if (err.name && err.message)
                    console.error(err.name + ": " + err.message);
                else console.error(err)
            });

        //requestAnimationFrame(CameraUtils.instance.tick);
    }



    /**
     * Inits the images matrices
     * for future processing
     */
    private initImages(): void {
        this.images = {
            rgb: new lcv.Mat(),
            undistorted: new lcv.Mat(),
            bw: new lcv.Mat(),
            binary: new lcv.Mat(),
            warp: new lcv.Mat(),
            final: new lcv.Mat()
        }
    }

    /**
     * Loads the aruco parameters for 
     * marker detection
     */
    private loadArucoParams(): void {
        this.detectorParams = new lcv.aruco_DetectorParameters();

        this.detectorParams.adaptiveThreshWinSizeMin = 3;
        this.detectorParams.adaptiveThreshWinSizeMax = 27;
        this.detectorParams.adaptiveThreshWinSizeStep = 8;
        this.detectorParams.adaptiveThreshConstant = 7;
        this.detectorParams.minMarkerPerimeterRate = 0.04;
        this.detectorParams.maxMarkerPerimeterRate = 3;
        this.detectorParams.polygonalApproxAccuracyRate = 0.02;
        this.detectorParams.minCornerDistanceRate = 0.05;
        this.detectorParams.minDistanceToBorder = 1;
        this.detectorParams.minMarkerDistanceRate = 0.05;
        this.detectorParams.markerBorderBits = 1;
        this.detectorParams.perspectiveRemovePixelPerCell = 7;
        this.detectorParams.perspectiveRemoveIgnoredMarginPerCell = 0.05;
        this.detectorParams.maxErroneousBitsInBorderRate = 0.1;
        this.detectorParams.minOtsuStdDev = 5;
        this.detectorParams.errorCorrectionRate = 0.3;
        this.detectorParams.detectInvertedMarker = true;

        // USELESS
        this.detectorParams.cornerRefinementMethod = lcv.CORNER_REFINE_NONE;
        this.detectorParams.cornerRefinementWinSize = 5;
        this.detectorParams.cornerRefinementMaxIterations = 200;
        this.detectorParams.cornerRefinementMinAccuracy = 0.01;
        /////
    }

    /**
     * Loads the camera distortion calibration values
     */
    public loadCalibration(): void {
        const w: number = 1280;
        const h: number = 720;

        const K = {
            rows: 3,
            cols: 3,
            dt: lcv.CV_32FC1,
            data: [687.2852563, 0., 649.20951191,
                0., 688.67506778, 332.46605063,
                0., 0., 1.]
        }

        const D = {
            rows: 1,
            cols: 5,
            dt: lcv.CV_32FC1,
            data: [-3.62992534e-01, 2.22580634e-01, 2.14890147e-03, 6.65814922e-05,
            -1.11815960e-01]
        }

        try {
            let mtx: number[] = new lcv.matFromArray(K.rows, K.cols, K.dt, K.data);
            let dist: number[] = new lcv.matFromArray(D.rows, D.cols, D.dt, D.data);

            const size = new lcv.Size(w, h);

            let res: any = lcv.getOptimalNewCameraMatrix(mtx, dist, size, 0, size);

            this.calibrations = {
                camMatrix: res,
                mtx: mtx,
                dist: dist,
            }
        } catch (error) {
            console.error(error);
        }

    }

    private loadArucoBoard(): void {
        // Sets the dictionary of aruco markers
        // in out case, original aruco
        this.dictionary = new lcv.aruco_Dictionary(lcv.DICT_ARUCO_ORIGINAL);

        // Ids of the markers on the keyboard in order
        const ids: any = lcv.matFromArray(1, 4, lcv.CV_32S, [0, 1, 2, 3]);

        // Calculate the objPoints in 3D coordinates of the keyboard based
        // on the marker size and the keyboard size
        const objPoints: any = new lcv.MatVector();
        objPoints.push_back(lcv.matFromArray(4, 3, lcv.CV_32F,
            [-this.keyboardW / 2, this.keyboardH / 2, 0,
            -this.keyboardW / 2 + this.markerLength, this.keyboardH / 2, 0,
            -this.keyboardW / 2 + this.markerLength, this.keyboardH / 2 - this.markerLength, 0,
            -this.keyboardW / 2, this.keyboardH / 2 - this.markerLength, 0]))

        objPoints.push_back(lcv.matFromArray(4, 3, lcv.CV_32F,
            [-this.keyboardW / 2, -this.keyboardH / 2 + this.markerLength, 0,
            -this.keyboardW / 2 + this.markerLength, -this.keyboardH / 2 + this.markerLength, 0,
            -this.keyboardW / 2 + this.markerLength, -this.keyboardH / 2, 0,
            -this.keyboardW / 2, -this.keyboardH / 2, 0]))

        objPoints.push_back(lcv.matFromArray(4, 3, lcv.CV_32F,
            [this.keyboardW / 2 - this.markerLength, -this.keyboardH / 2 + this.markerLength, 0,
            this.keyboardW / 2, -this.keyboardH / 2 + this.markerLength, 0,
            this.keyboardW / 2, -this.keyboardH / 2, 0,
            this.keyboardW / 2 - this.markerLength, -this.keyboardH / 2, 0]))

        objPoints.push_back(lcv.matFromArray(4, 3, lcv.CV_32F,
            [this.keyboardW / 2 - this.markerLength, this.keyboardH / 2, 0,
            this.keyboardW / 2, this.keyboardH / 2, 0,
            this.keyboardW / 2, this.keyboardH / 2 - this.markerLength, 0,
            this.keyboardW / 2 - this.markerLength, this.keyboardH / 2 - this.markerLength, 0]))

        this.keyboardBoard = new lcv.aruco_Board(objPoints, this.dictionary, ids);
    }

    /**
     * Called at each tick of the camera,
     * calculates the markers positions
     */
    public async tick(): Promise<void> {
        try {
            if (this.video.readyState === this.video.HAVE_ENOUGH_DATA) {
                this.capture.read(this.inputImage);

                // Undistort image
                this.undistortImage(this.inputImage);

                // Object definition
                const markerIds = new lcv.Mat();
                const markerCorners = new lcv.MatVector();
                const rejectedCandidates = new lcv.MatVector();

                // Image colorspaces 
                lcv.cvtColor(this.images.undistorted, this.images.rgb, lcv.COLOR_RGBA2RGB, 0);
                lcv.cvtColor(this.images.undistorted, this.images.bw, lcv.COLOR_RGBA2GRAY, 0);

                // Image processing for better marker recognizing
                lcv.GaussianBlur(this.images.bw, this.images.binary, new lcv.Size(0, 0), 4);
                lcv.addWeighted(this.images.bw, 2, this.images.binary, -1.5, 0, this.images.binary);
                lcv.equalizeHist(this.images.binary, this.images.binary);
                lcv.threshold(this.images.binary, this.images.binary, 0, 0, lcv.THRESH_TOZERO);
                lcv.threshold(this.images.binary, this.images.binary, 220, 255, lcv.THRESH_BINARY);
                //lcv.medianBlur(binaryImg, binaryImg, 3);

                // Detect markers
                lcv.detectMarkers(this.images.binary, this.dictionary, markerCorners, markerIds, this.detectorParams, rejectedCandidates, this.calibrations.camMatrix, this.calibrations.dist);
                lcv.refineDetectedMarkers(this.images.binary, this.keyboardBoard, markerCorners, markerIds, rejectedCandidates, this.calibrations.camMatrix, this.calibrations.dist);

                // Draw markers
                const markRvects = new lcv.Mat();
                const markTvects = new lcv.Mat();
                const points3D = new lcv.Mat();

                if (markerIds.rows > 0) {

                    // Estimate board position
                    lcv.estimatePoseBoard(markerCorners, markerIds, this.keyboardBoard, this.calibrations.camMatrix, this.calibrations.dist, markRvects, markTvects);

                    let keyboardRotEulerMatrix: any = new lcv.Mat(3, 3, lcv.CV_64F);
                    lcv.Rodrigues(markRvects, keyboardRotEulerMatrix);

                    this.keyboardCoord = {
                        T: {
                            x: markTvects.data64F[0] * 10,
                            y: markTvects.data64F[1],
                            z: -markTvects.data64F[2] * 5,
                        },
                        R: this.getRotationEulersValues(keyboardRotEulerMatrix)
                    }


                    // If all 4 markers are detected, calculate camera position
                    if ((markerCorners.size() >= 2 && Object.keys(this.markerPoints).length == 4) || markerCorners.size() >= 4) {

                        if (!this.initialPosCalibrated) {
                            this.initialPosCalibrated = true;

                            const object3Dpoints: any = new lcv.Mat();
                            const boardMarkerCorners: any = new lcv.Mat();

                            // Get board object and image points matrices for solvePnP
                            lcv.getBoardObjectAndImagePoints(this.keyboardBoard, markerCorners, markerIds, object3Dpoints, boardMarkerCorners);

                            // Estimate camera position base on board position
                            const inliers: any = new lcv.Mat();
                            lcv.solvePnP(object3Dpoints, boardMarkerCorners, this.calibrations.camMatrix, this.calibrations.dist, this.camRvect, this.camTvect, false);

                            // Set the camera rotation values for rendering
                            let camRotEulerMatrix: any = new lcv.Mat(3, 3, lcv.CV_64F);
                            lcv.Rodrigues(this.camRvect, camRotEulerMatrix);

                            this.camCoord = {
                                T: {
                                    x: this.camTvect.data64F[0] * 15,
                                    y: this.camTvect.data64F[1] * 10,
                                    z: -this.camTvect.data64F[2] * 5,
                                },
                                R: this.getRotationEulersValues(camRotEulerMatrix)
                            }

                            //camRotEulerMatrix.delete();
                            inliers.delete();
                            object3Dpoints.delete();
                            boardMarkerCorners.delete();
                        }

                        //
                        // --- Edge detection for the keyboard
                        // check the median point for each marker and get the right outer point 
                        // based on median point absolute position 
                        //
                        // Algorithm:
                        // 1. Calculate the mean coordinates for each marker of the 4 corners
                        // 2. Sorts them by the mean value of x to get the most left and the
                        //    most right markers
                        // 3. Sort each of them by the value of y to get the most top and the
                        //    most bottom of each couple
                        // 4. Get the first corner of the first marker, second of second etc...
                        //


                        // structure creation and mean point calculation
                        for (let i = 0; i < markerCorners.size(); i++) {
                            const mark: Point2D[] = []
                            let totX = 0;
                            let totY = 0;
                            for (let j = 0; j < 4; j++) {
                                let point: Point2D = {
                                    x: markerCorners.get(i).col(j).data32F[0],
                                    y: markerCorners.get(i).col(j).data32F[1]
                                };
                                totX += markerCorners.get(i).col(j).data32F[0]
                                totY += markerCorners.get(i).col(j).data32F[1]

                                mark.push(point);
                            }
                            this.markerPoints[markerIds.data32S[i]] = {
                                points: mark,
                                meanPoint: {
                                    x: totX / 4,
                                    y: totY / 4,
                                }
                            };
                        }

                        let markerArray: {
                            points: Point2D[],
                            meanPoint: Point2D
                        }[] = [];
                        for (let i = 0; i < 4; i++) {
                            markerArray.push(this.markerPoints[i]);
                        }
                        console.log(markerArray);
                        // left and right marker subdivision
                        markerArray.sort((p1, p2) => {
                            return p1.meanPoint.x - p2.meanPoint.x;
                        })

                        const leftMarkerPoints = [markerArray[0], markerArray[1]];
                        const rightMarkerPoints = [markerArray[2], markerArray[3]];

                        // upper and lower marker subdivision
                        leftMarkerPoints.sort((p1, p2) => {
                            return p1.meanPoint.y - p2.meanPoint.y;
                        });
                        rightMarkerPoints.sort((p1, p2) => {
                            return p1.meanPoint.y - p2.meanPoint.y;
                        });

                        const TLM: Point2D[] = leftMarkerPoints[0].points;
                        const BLM: Point2D[] = leftMarkerPoints[1].points;
                        const TRM: Point2D[] = rightMarkerPoints[0].points;
                        const BRM: Point2D[] = rightMarkerPoints[1].points;

                        // board corner assignment
                        const TL: Point2D = TLM[0];
                        const TR: Point2D = TRM[1];
                        const BR: Point2D = BRM[2];
                        const BL: Point2D = BLM[3];

                        let boardPoints: any = lcv.matFromArray(4, 2, lcv.CV_32F, [
                            TL.x, TL.y,
                            TR.x, TR.y,
                            BR.x, BR.y,
                            BL.x, BL.y,
                        ]);

                        // Calculate keyboardDst based on the max width and the max height detected
                        const widthA = Math.sqrt(Math.pow(BR.x - BL.x, 2) + Math.pow(BR.y - BL.y, 2));
                        const widthB = Math.sqrt(Math.pow(TR.x - TL.x, 2) + Math.pow(TR.y - TL.y, 2));
                        const maxWidth = Math.max(widthA, widthB);

                        const heightA = Math.sqrt(Math.pow(TR.x - BR.x, 2) + Math.pow(TR.y - BR.y, 2));
                        const heightB = Math.sqrt(Math.pow(TL.x - BL.x, 2) + Math.pow(TL.y - BL.y, 2));
                        const maxHeight = Math.max(heightA, heightB);

                        // keyboard destination matrix where 5 is used as offset to get some border
                        this.keyboardDst = lcv.matFromArray(4, 2, lcv.CV_32F, [
                            5, 5,
                            maxWidth - 5, 5,
                            maxWidth - 5, maxHeight - 5,
                            5, maxHeight - 5,
                        ]);

                        // warp camera image to get always a rectified keyboard image
                        const M = lcv.getPerspectiveTransform(boardPoints, this.keyboardDst)
                        lcv.warpPerspective(this.images.rgb, this.images.warp, M, new lcv.Size(1280, 720))

                        // crop the rectangle where the keyboard is located and transfer it on keyboard dedicated canvas
                        const rect = new lcv.Rect(0, 0, maxWidth, maxHeight);
                        this.images.final = this.images.warp.roi(rect);

                        lcv.imshow("canvas", this.images.final);

                        boardPoints.delete();
                    }

                    // Draw detected markers on Rgb Image
                    lcv.drawDetectedMarkers(this.images.rgb, markerCorners, markerIds);
                    lcv.drawAxis(this.images.rgb, this.calibrations.camMatrix, this.calibrations.dist, markRvects, markTvects, 0.01);

                }
                lcv.imshow("outCanvas", this.images.rgb);
                lcv.imshow("binaryCanvas", this.images.binary);
                rejectedCandidates.delete();
                markRvects.delete();
                markTvects.delete();
                points3D.delete();
                markerIds.delete();
                markerCorners.delete();
            }
        } catch (e) {
            console.error(e);
        }
    }

    /**
     * Undistorts the input image base on
     * the calibration matrix
     */
    private undistortImage(inputImage: any): any {
        if (this.calibrations && this.calibrations.mtx && this.calibrations.dist && this.calibrations.camMatrix) {
            lcv.undistort(inputImage, this.images.undistorted, this.calibrations.mtx, this.calibrations.dist, this.calibrations.camMatrix);
        }
    }

    /**
     * Calculates the rotation eulers angles
     * given the rvect of an element
     * 
     * @param R 
     * @param forceY 
     * @returns 
     */
    private getRotationEulersValues(R: any, forceY: boolean = false): { thetaX: number, thetaY: number, thetaZ: number } {

        const cameraMatrix = new lcv.Mat();
        const rotMatrix = new lcv.Mat();
        const transVect = new lcv.Mat();
        const rotMatrixX = new lcv.Mat();
        const rotMatrixY = new lcv.Mat();
        const rotMatrixZ = new lcv.Mat();

        const eulerAngles = new lcv.Mat();

        const projMatrix = lcv.matFromArray(3, 4, lcv.CV_32F, [
            R.doubleAt(0, 0), R.doubleAt(0, 1), R.doubleAt(0, 2), 0,
            R.doubleAt(1, 0), R.doubleAt(1, 1), R.doubleAt(1, 2), 0,
            R.doubleAt(2, 0), R.doubleAt(2, 1), R.doubleAt(2, 2), 0,
        ]);

        lcv.decomposeProjectionMatrix(projMatrix,
            cameraMatrix,
            rotMatrix,
            transVect,
            rotMatrixX,
            rotMatrixY,
            rotMatrixZ,
            eulerAngles);

        console.log(eulerAngles);

        cameraMatrix.delete();
        rotMatrix.delete();
        transVect.delete();
        rotMatrixX.delete();
        rotMatrixY.delete();
        rotMatrixZ.delete();

        return {
            thetaX: eulerAngles.doubleAt(0, 0),
            thetaY: eulerAngles.doubleAt(0, 1),
            thetaZ: eulerAngles.doubleAt(0, 2)
        }
        /*const sy: number = Math.sqrt(R.doubleAt(0, 0) * R.doubleAt(0, 0) + R.doubleAt(1, 0) * R.doubleAt(1, 0));
        const singular: boolean = sy < 1e-6;
      
        let x: number, y: number, z: number;
        if (!singular && !forceY) {
            x = Math.atan2(R.doubleAt(2, 1), R.doubleAt(2, 2));
            y = Math.atan2(R.doubleAt(2, 0), sy);
            z = Math.atan2(R.doubleAt(1, 0), R.doubleAt(0, 0));
        } else {
            x = Math.atan2(R.doubleAt(1, 2), R.doubleAt(1, 1));
            y = Math.atan2(R.doubleAt(2, 0), sy);
            z = 0;
        }
      
      
        return {
            thetaX: (x * 360) / (2 * Math.PI),
            thetaY: (y * 360) / (2 * Math.PI),
            thetaZ: (z * 360) / (2 * Math.PI)
        }*/
    }
}