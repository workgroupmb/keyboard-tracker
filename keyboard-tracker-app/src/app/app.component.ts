import { Component, OnInit } from '@angular/core';
import { CameraUtils } from './CameraUtils';

declare let cv: any;

declare let AFRAME: any;
export let lcv: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  private camera: CameraUtils;

  private res = {
    w: 1920,
    h: 1080
  }

  constructor() {
    this.camera = CameraUtils.instance;
  }

  ngOnInit(): void {

    cv.then((val: any) => {
      lcv = val;
      //this.onLoad();
    });

    //
    // ----- AFRAME CANVAS COMPONENT
    // adds a material based on a canvas
    //
    AFRAME.registerComponent('canvas-updater', {
      dependencies: ['geometry', 'material'],
      tick: function () {
        var el = this.el;
        var material;
        material = el.getObject3D('mesh').material;
        if (!material.map) { return; }
        material.map.needsUpdate = true;

      }
    });
    //
    // ----- AFRAME CAMERA COMPONENT
    // set attributes for camera position and rotation
    //
    AFRAME.registerComponent('mb-camera', {
      tick: function () {
        var el = this.el;
        const thetaX: number = CameraUtils.instance.camCoord.R.thetaX;
        const thetaY: number = CameraUtils.instance.camCoord.R.thetaY;
        const thetaZ: number = CameraUtils.instance.camCoord.R.thetaZ;

        el.setAttribute('rotation', { x: thetaX, y: thetaY, z: thetaZ });
        el.setAttribute('position', CameraUtils.instance.camCoord.T);
      }
    })

    //
    // ----- AFRAME KEYBOARD COMPONENT
    // set attributes for keyboard position and rotation
    //
    AFRAME.registerComponent('mb-keyboard', {
      schema: {
        id: { type: 'number' },
      },
      tick: function () {
        const el = this.el;
        const thetaX: number = CameraUtils.instance.keyboardCoord.R.thetaX;
        const thetaY: number = CameraUtils.instance.keyboardCoord.R.thetaY;
        const thetaZ: number = CameraUtils.instance.keyboardCoord.R.thetaZ;

        el.setAttribute('rotation', { x: thetaX, y: thetaY, z: thetaZ });
        el.setAttribute('position', CameraUtils.instance.keyboardCoord.T);
      }
    })
  }

  public onLoad(): void {
    const video: any = document.getElementById("video");
    const canvas: any = document.getElementById("canvas");
    const outCanvas: any = document.getElementById("outCanvas");

    canvas.width = parseInt(canvas.style.width);
    canvas.height = parseInt(canvas.style.height);

    outCanvas.width = parseInt(outCanvas.style.width);
    outCanvas.height = parseInt(outCanvas.style.height);

    CameraUtils.instance.onLoad(video);
  }

}
