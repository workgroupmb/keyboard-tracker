import cv2
from cv2 import aruco
import numpy as np
import math
from collections import namedtuple
import time

'''
  image     - image to track
  rect      - tracked rectangle (x1, y1, x2, y2)
  keypoints - keypoints detected inside rect
  descrs    - their descriptors
  data      - some user-provided data
'''
PlanarTarget = namedtuple('PlaneTarget', 'image, rect, keypoints, descrs, data')

'''
  target - reference to PlanarTarget
  p0     - matched points coords in target image
  p1     - matched points coords in input frame
  H      - homography matrix from p0 to p1
  quad   - target boundary quad in input frame
'''
TrackedTarget = namedtuple('TrackedTarget', 'target, p0, p1, H, quad')

FLANN_INDEX_KDTREE = 1
FLANN_INDEX_LSH    = 6
flann_params= dict(algorithm = FLANN_INDEX_LSH,
                   table_number = 6, # 12
                   key_size = 12,     # 20
                   multi_probe_level = 1) #2

MIN_MATCH_COUNT = 10

class CameraUtils:
    keyboard_coord = dict({
        "T": dict({
            "x": 0,
            "y": 0,
            "z": 0
        }),
        "R": dict({
            "thetaX": 0,
            "thetaY": 0,
            "thetaZ": 0
        })
    })

    cam_coord = dict({
        "T": dict({
            "x": 0,
            "y": 0,
            "z": 0
        }),
        "R": dict({
            "thetaX": 0,
            "thetaY": 0,
            "thetaZ": 0
        })
    })

    marker_points = dict({})

    imgW = 1280
    imgH = 720
    initial_pos_calibrated = False

    markerLength = 0.025
    keyboardW = 0.1
    keyboardH = 0.405

    ##### Initializer #####
    def __init__(self):
        cv2.ocl.setUseOpenCL(True)
        self.load_calibration()
        self.load_aruco_params()
        self.load_aruco_board()
        self.init_images()

        self.detector = cv2.ORB_create( nfeatures = 1000 )
        self.matcher = cv2.FlannBasedMatcher(flann_params, {})


    ##### LOAD CALIBRATION #####
    def load_calibration(self):
        mtx = np.array([
            [687.2852563, 0., 649.20951191],
            [0., 688.67506778, 332.46605063],
            [0., 0., 1.]
        ])
        dist = np.array([-3.62992534e-01, 2.22580634e-01,
                         2.14890147e-03, 6.65814922e-05, -1.11815960e-01])

        newcameramtx, roi = cv2.getOptimalNewCameraMatrix(
            mtx, dist, (self.imgW, self.imgH), 0, (self.imgW, self.imgH))

        self.calibrations = {
            "camMatrix": newcameramtx,
            "mtx": mtx,
            "dist": dist,
        }

    ##### INIT IMAGES #####
    def init_images(self):

        self.images = {
            "rgb": None,
            "undistorted": None,
            "bw": None,
            "binary": None,
            "warp": None,
            "final": None
        }

    ##### LOAD ARUCO PARAMS #####
    def load_aruco_params(self):
        self.detectorParams = aruco.DetectorParameters_create()

        self.detectorParams.adaptiveThreshWinSizeMin = 3
        self.detectorParams.adaptiveThreshWinSizeMax = 27
        self.detectorParams.adaptiveThreshWinSizeStep = 8
        self.detectorParams.adaptiveThreshConstant = 7
        self.detectorParams.minMarkerPerimeterRate = 0.04
        self.detectorParams.maxMarkerPerimeterRate = 3
        self.detectorParams.polygonalApproxAccuracyRate = 0.02
        self.detectorParams.minCornerDistanceRate = 0.05
        self.detectorParams.minDistanceToBorder = 1
        self.detectorParams.minMarkerDistanceRate = 0.05
        self.detectorParams.markerBorderBits = 1
        self.detectorParams.perspectiveRemovePixelPerCell = 7
        self.detectorParams.perspectiveRemoveIgnoredMarginPerCell = 0.05
        self.detectorParams.maxErroneousBitsInBorderRate = 0.1
        self.detectorParams.minOtsuStdDev = 5
        self.detectorParams.errorCorrectionRate = 0.3
        self.detectorParams.detectInvertedMarker = True
        # USELESS
        self.detectorParams.cornerRefinementMethod = aruco.CORNER_REFINE_NONE
        self.detectorParams.cornerRefinementWinSize = 5
        self.detectorParams.cornerRefinementMaxIterations = 200
        self.detectorParams.cornerRefinementMinAccuracy = 0.01

    ##### LOAD ARUCO BOARD #####
    def load_aruco_board(self):
        # Sets the dictionary of aruco markers
        # in out case, original aruco
        self.dictionary = aruco.Dictionary_get(aruco.DICT_ARUCO_ORIGINAL)

        # Ids of the markers on the keyboard in order
        ids = np.array([0,1,2,3])

        # Calculate the objPoints in 3D coordinates of the keyboard based
        # on the marker size and the keyboard size
        objPoints = np.float32([
            [[-self.keyboardW / 2, self.keyboardH / 2, 0],
             [-self.keyboardW / 2 + self.markerLength, self.keyboardH / 2, 0],
             [-self.keyboardW / 2 + self.markerLength,
                self.keyboardH / 2 - self.markerLength, 0],
             [-self.keyboardW / 2, self.keyboardH / 2 - self.markerLength, 0]],

            [[-self.keyboardW / 2, -self.keyboardH / 2 + self.markerLength, 0],
             [-self.keyboardW / 2 + self.markerLength, -
                self.keyboardH / 2 + self.markerLength, 0],
             [-self.keyboardW / 2 + self.markerLength, -self.keyboardH / 2, 0],
             [-self.keyboardW / 2, -self.keyboardH / 2, 0]],

            [[self.keyboardW / 2 - self.markerLength, -self.keyboardH / 2 + self.markerLength, 0],
             [self.keyboardW / 2, -self.keyboardH / 2 + self.markerLength, 0],
             [self.keyboardW / 2, -self.keyboardH / 2, 0],
             [self.keyboardW / 2 - self.markerLength, -self.keyboardH / 2, 0]],

            [[self.keyboardW / 2 - self.markerLength, self.keyboardH / 2, 0],
             [self.keyboardW / 2, self.keyboardH / 2, 0],
             [self.keyboardW / 2, self.keyboardH / 2 - self.markerLength, 0],
             [self.keyboardW / 2 - self.markerLength, self.keyboardH / 2 - self.markerLength, 0]]
        ])

        """ objPoints = np.float32([
            [[0,0,0],
             [self.markerLength, 0,0],
             [self.markerLength,self.markerLength,0],
             [0,self.markerLength,0]],
            
            [[0, self.keyboardH-self.markerLength,0],
             [self.markerLength, self.keyboardH-self.markerLength, 0],
             [self.markerLength, self.keyboardH, 0],
             [0, self.keyboardH,0]],

            [[self.keyboardW-self.markerLength, self.keyboardH-self.markerLength,0],
             [self.keyboardW, self.keyboardH-self.markerLength,0],
             [self.keyboardW, self.keyboardH, 0],
             [self.keyboardW-self.markerLength, self.keyboardH,0]],

            [[self.keyboardW-self.markerLength, 0,0],
             [self.keyboardW,0,0],
             [self.keyboardW, self.markerLength,0],
             [self.keyboardW-self.markerLength, self.markerLength,0]],
        ])  """
        self.keyboardBoard = aruco.Board_create(objPoints, self.dictionary, ids)
        #print(self.keyboardBoard.objPoints)

    def undistort_image(self, input_image):
        return cv2.undistort(input_image, self.calibrations["mtx"],
                             self.calibrations["dist"], None, self.calibrations["camMatrix"])


    def clearMatch(self):
        '''Remove all targets'''
        self.target = None
        self.matcher.clear()
    
    
    def detect_features(self, frame):
        '''detect_features(self, frame) -> keypoints, descrs'''
        keypoints, descrs = self.detector.detectAndCompute(frame, None)
        if descrs is None:  # detectAndCompute returns descs=None if not keypoints found
            descrs = []
        return keypoints, descrs

    def track(self, frame):
        '''Returns a list of detected TrackedTarget objects'''
        frame_points, frame_descrs = self.detect_features(frame)
        matches = self.matcher.knnMatch(frame_descrs, k = 2)
        matches = [m[0] for m in matches if len(m) == 2 and m[0].distance < m[1].distance * 0.75]
        if len(matches) < MIN_MATCH_COUNT:
            return []
                
        p0 = [self.target.keypoints[m.trainIdx].pt for m in matches]
        p1 = [frame_points[m.queryIdx].pt for m in matches]
        p0, p1 = np.float32((p0, p1))
        H, status = cv2.findHomography(p0, p1, cv2.RANSAC, 3.0)
        status = status.ravel() != 0
        
        p0, p1 = p0[status], p1[status]

        pt0, pt1, pt2, pt3 = self.target.rect
        quad = np.float32([[pt0[0],pt0[1]], [pt1[0],pt1[1]], [pt2[0],pt2[1]], [pt3[0],pt3[1]]])
        quad = cv2.perspectiveTransform(quad.reshape(1, -1, 2), H).reshape(-1, 2)

        track = TrackedTarget(target=self.target, p0=p0, p1=p1, H=H, quad=quad)
        return track

    """ 
    Calculates the rotation eulers angles
    given the rvect of an element
    """
    def get_rotation_eulers_values(self, R):
        R = cv2.Rodrigues(R)[0]

        roll = 180*math.atan2(-R[2][1], R[2][2])/math.pi
        pitch = 180*math.asin(R[2][0])/math.pi
        yaw = 180*math.atan2(-R[1][0], R[0][0])/math.pi

        return dict({
            "thetaX": roll,
            "thetaY": pitch,
            "thetaZ": yaw
        })

    def set_target(self, frame, board_points, data=None):
        raw_points, raw_descrs = self.detect_features(frame)
        points, descs = [], []
        for kp, desc in zip(raw_points, raw_descrs):
            if cv2.pointPolygonTest(board_points, kp.pt, False)>0:
                points.append(kp)
                descs.append(desc)
        descs = np.uint8(descs)
        self.matcher.add([descs])
        self.target = PlanarTarget(image = frame, rect=board_points, keypoints = points, descrs=descs, data=data)

    def get_board_coords(self, input_image):
        self.images["undistorted"] = self.undistort_image(input_image)

        # Image colorspaces
        self.images["rgb"] = cv2.cvtColor(
            self.images["undistorted"], cv2.COLOR_RGBA2RGB)
        self.images["bw"] = cv2.cvtColor(
            self.images["undistorted"], cv2.COLOR_RGBA2GRAY)

        # Image processing for better marker recognizing
        self.images["binary"] = cv2.GaussianBlur(self.images["bw"], (0, 0), 4)

        self.images["binary"] = cv2.addWeighted(
            self.images["bw"], 2, self.images["binary"], -1.5, 0)

        self.images["binary"] = cv2.equalizeHist(self.images["binary"])

        _, self.images["binary"] = cv2.threshold(
            self.images["binary"], 0, 0, cv2.THRESH_TOZERO)

        _, self.images["binary"] = cv2.threshold(
            self.images["binary"], 220, 255, cv2.THRESH_BINARY)

        # Detect markers
        marker_corners, marker_ids, rejected_img_points = aruco.detectMarkers(
            self.images["binary"], self.dictionary, None, None, self.detectorParams, None, self.calibrations["camMatrix"], self.calibrations["dist"])

        """ marker_corners, marker_ids, rejected_img_points, rec_index = aruco.refineDetectedMarkers(
            self.images["binary"], self.keyboardBoard, marker_corners, marker_ids, rejected_img_points) """

        # 1. Calibrazione iniziale / periodica quando vengono trovati i 4 marker
        if (marker_ids is not None and len(marker_ids) >= 1):
            if (len(marker_ids) >= 4):
                t0 = time.time()
                print("FOUND 4 MARKERS, RECALIBRATION")
                
                """ undetectedIds= []
                undetectedCorners = []
                for i in range (0,len(self.keyboardBoard.ids)):
                    foundIdx=-1
                    for j in range(0, len(marker_ids)):
                        if(self.keyboardBoard.ids[i][0] == marker_ids[j][0]):
                            foundIdx = j
                            break

                    # not detected
                    if(foundIdx == -1):
                        undetectedIds.append(self.keyboardBoard.ids[i][0])
                        points, jacobian = cv2.projectPoints(self.keyboardBoard.objPoints[i][0], mark_r_vects, mark_t_vects, self.calibrations["camMatrix"], self.calibrations["dist"])
                        undetectedCorners.append(points)
                        self.images["rgb"] = cv2.circle(self.images["rgb"], (points[0][0][0], points[0][0][1]), radius=2, color=(0, 0, 255), thickness=2)

                print(undetectedIds)
                print(undetectedCorners)
                print(marker_corners)

                print("----------") """
                    
                """ self.keyboard_coord["T"]["x"] = mark_t_vects[0][0]
                self.keyboard_coord["T"]["y"] = mark_t_vects[1][0]
                self.keyboard_coord["T"]["z"] = mark_t_vects[2][0]

                self.keyboard_coord["R"] = self.get_rotation_eulers_values(mark_r_vects)"""


                # First calibration for camera pose estimation - executed just once
                if(self.initial_pos_calibrated == False):
                    self.initial_pos_calibrated = True

                    retval, mark_r_vects, mark_t_vects = aruco.estimatePoseBoard(
                        marker_corners, marker_ids, self.keyboardBoard, self.calibrations["camMatrix"], self.calibrations["dist"], None, None)

                    # Get board object and image points matrices for solvePnP
                    obj_points, img_points = aruco.getBoardObjectAndImagePoints(
                        self.keyboardBoard, marker_corners, marker_ids)
                        
                    # Estimate camera position base on board position
                    retval, rvec, tvec = cv2.solvePnP(
                        obj_points, img_points, self.calibrations["camMatrix"], self.calibrations["dist"])

                    #img_points, jacobian = cv2.projectPoints(obj_points, rvec, tvec, self.calibrations["camMatrix"], self.calibrations["dist"])

                    # Set the camera rotation values for rendering
                    cam_rot_euler_matrix = cv2.Rodrigues(rvec)
                    self.cam_coord["T"]["x"] = tvec[0][0]
                    self.cam_coord["T"]["y"] = tvec[1][0]
                    self.cam_coord["T"]["z"] = tvec[2][0]

                    self.cam_coord["R"] = self.get_rotation_eulers_values(mark_r_vects)
            
                """  
                    --- Edge detection for the keyboard
                    check the median point for each marker and get the right outer point 
                    based on median point absolute position 
                    
                    Algorithm:
                    1. Calculate the mean coordinates for each marker of the 4 corners
                    2. Sorts them by the mean value of x to get the most left and the
                        most right markers
                    3. Sort each of them by the value of y to get the most top and the
                        most bottom of each couple
                    4. Get the first corner of the first marker, second of second etc...
                """

                # structure creation and mean point calculation
                for i in range(0, len(marker_corners)):
                    mark = []
                    tot_x = 0
                    tot_y = 0

                    for j in range(0, 4):
                        point = dict({
                            "x": marker_corners[i][0][j][0],
                            "y": marker_corners[i][0][j][1]
                        })

                        tot_x += marker_corners[i][0][j][0]
                        tot_y += marker_corners[i][0][j][1]

                        mark.append(point)

                    self.marker_points[marker_ids[i][0]] = dict({
                        "points": mark,
                        "mean_point": dict({
                            "x": tot_x / 4,
                            "y": tot_y / 4
                        })
                    })

                marker_array = []

                for i in range(0, 4):
                    marker_array.append(self.marker_points[i])

                marker_array = sorted(
                    marker_array, key=lambda marker: marker["mean_point"]["x"])

                left_mark_points = [
                    marker_array[0], marker_array[1]
                ]
                right_mark_points = [
                    marker_array[2], marker_array[3]
                ]

                left_mark_points = sorted(
                    left_mark_points, key=lambda marker: marker["mean_point"]["y"])
                right_mark_points = sorted(
                    right_mark_points, key=lambda marker: marker["mean_point"]["y"])

                TLM = left_mark_points[0]["points"]
                BLM = left_mark_points[1]["points"]
                TRM = right_mark_points[0]["points"]
                BRM = right_mark_points[1]["points"]

                TL = TLM[0]
                TR = TRM[1]
                BR = BRM[2]
                BL = BLM[3]

                board_points = np.array([
                    [TL["x"], TL["y"]],
                    [TR["x"], TR["y"]],
                    [BR["x"], BR["y"]],
                    [BL["x"], BL["y"]],
                ])

                width_A = math.sqrt((BR["x"] - BL["x"])
                                    ** 2 + (BR["y"] - BL["y"])**2)
                width_B = math.sqrt((TR["x"] - TL["x"])
                                    ** 2 + (TR["y"] - TL["y"])**2)

                max_width = math.floor(max(width_A, width_B))

                height_A = math.sqrt(
                    (TR["x"] - BR["x"])**2 + (TR["y"] - BR["y"])**2)
                height_B = math.sqrt(
                    (TL["x"] - BL["x"])**2 + (TL["y"] - BL["y"])**2)
                max_height = math.floor(max(height_A, height_B))

                # keyboard destination matrix where 5 is used as offset to get some border
                self.keyboardDst = np.float32([
                    [5, 5],
                    [max_width - 5, 5],
                    [max_width - 5, max_height - 5],
                    [5, max_height - 5],
                ])


                # warp camera image to get always a rectified keyboard image
                """ M = cv2.getPerspectiveTransform(board_points, self.keyboardDst)
                self.images["warp"] = cv2.warpPerspective(
                    self.images["rgb"], M, (self.imgW, self.imgH))
                # crop the rectangle where the keyboard is located and transfer it on keyboard dedicated canvas
                self.images["warp"] = self.images["warp"][0:
                                                            max_height, 0:max_width] """

                # 2. Se la calibrazione è stata effettuata allora estrai le feature
                self.clearMatch()
                self.set_target(self.images["binary"], board_points)

                # TODO: Da fare su un thread separato con doppio valore per salvare il target dopo il calcolo

                print("Recalibration took", time.time()-t0)

                # DEVELOPING UTILITY
                """ aruco.drawDetectedMarkers(
                    self.images["rgb"], marker_corners, marker_ids)
                aruco.drawAxis(self.images["rgb"], self.calibrations["camMatrix"],
                            self.calibrations["dist"], mark_r_vects, mark_t_vects, 0.01) """
                # ------

            # 3. Feature detection per la posizione del piano
            if(self.initial_pos_calibrated):
                tracked = self.track(self.images["binary"])
                cv2.polylines(self.images["rgb"], [np.int32(tracked.quad)], True, (255, 255, 255), 2)
                for (x, y) in np.int32(tracked.p1):
                    cv2.circle(self.images["rgb"], (x, y), 2, (255, 255, 255))


        # 5. Salvataggio informazioni
        
        
        cv2.imshow('imagergb', self.images["rgb"])
        cv2.imshow('imagebinary', self.images["binary"])
        cv2.waitKey(1)
        return (self.keyboard_coord, self.cam_coord)
