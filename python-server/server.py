#!/usr/bin/env python
from importlib import import_module
import os
from flask import Flask, render_template, Response

from camera import Camera

import time
from flask_socketio import SocketIO, send, emit

import json

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)


@app.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html')


def gen(camera):
    """Video streaming generator function."""
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@socketio.on('my event')
def handle_my_custom_event(msg):
    while True:
        send(json.dumps({
            "keyboard_c": Camera.kc,
            "camera_c": Camera.cc
        }))
        time.sleep(0.016)


@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(Camera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    socketio.run(app)
