import os
import cv2
from basecamera import BaseCamera
from camera_utils import CameraUtils
import time


class Camera(BaseCamera):
    video_source = 0
    camera_utils = CameraUtils()
    kc = None
    cc = None

    time_count = 1
    time_count_max = 1

    def __init__(self):
        if os.environ.get('OPENCV_CAMERA_SOURCE'):
            Camera.set_video_source(int(os.environ['OPENCV_CAMERA_SOURCE']))
        super(Camera, self).__init__()

    @staticmethod
    def set_video_source(source):
        Camera.video_source = source

    @staticmethod
    def frames():
        camera = cv2.VideoCapture(Camera.video_source)
        if not camera.isOpened():
            raise RuntimeError('Could not start camera.')

        mean_time_sum = 0
        while True:
            Camera.time_count -= 1
            # read current frame
            _, img = camera.read()
            tim = time.time()
            Camera.kc, Camera.cc = Camera.camera_utils.get_board_coords(img)
            rgbimg = Camera.camera_utils.images["rgb"]

            mean_time_sum += (time.time()-tim)
            if (Camera.time_count <= 0):
                print("Mean elapsed time:", mean_time_sum/Camera.time_count_max)
                mean_time_sum = 0
                Camera.time_count = Camera.time_count_max

            # encode as a jpeg image and return it
            yield cv2.imencode('.jpg', rgbimg)[1].tobytes()
